gpg --no-default-keyring --keyring .\CRESCHZF.gpg --fingerprint
gpg --no-default-keyring --keyring .\CRESCHZF.gpg --gen-key
REM user CRESCHZF, email security@CRESCHZF.com
gpg --no-default-keyring --keyring .\CRESCHZF.gpg --list-keys
gpg --no-default-keyring --keyring .\CRESCHZF.gpg --armor --export CRESCHZF > publicCRESCHZF.asc
REM => export public key of CRESCHZF to file publicCRESCHZF.asc in ascii format, this is to be insterted into clob fiel of DB