REM
echo off
ECHO "DELETING THE LOG FILES..."
FOR /R . %%X IN (*.log) DO (del /s /q "%%X" 2>nul)

ECHO "DELETING THE DEBUG FILES..."
FOR /R . %%X IN (*.debug) DO (del /s /q "%%X" 2>nul)
FOR /R . %%X IN (201*.msgdef.xml) DO (del /s /q "%%X" 2>nul)

ECHO "DELETING THE FAILED FILES..."
FOR /R . %%X IN (*.failed) DO (del /s /q "%%X" 2>nul)
FOR /R . %%X IN (*.failedFromMQ) DO (del /s /q "%%X" 2>nul)
FOR /R . %%X IN (*.failedRouting) DO (del /s /q "%%X" 2>nul)
FOR /R . %%X IN (*.out) DO (del /s /q "%%X" 2>nul)
FOR /R . %%X IN (*.dump) DO (del /s /q "%%X" 2>nul)

del .\data\genBusImport\csv\done\*.* /s /q
