gpg --no-default-keyring --keyring .\ZURICHZF.gpg --fingerprint
gpg --no-default-keyring --keyring .\ZURICHZF.gpg --gen-key
user ZURICHZF, email security@ZURICHZF.com
gpg --no-default-keyring --keyring .\ZURICHZF.gpg --list-keys
gpg --no-default-keyring --keyring .\ZURICHZF.gpg --armor --export ZURICHZF > publicZURICHZF.asc
=> export public key of ZURICHZF to file publicZURICHZF.asc in ascii format, this is to be insterted into clob fiel of DB
gpg --no-default-keyring --keyring .\ZURICHZF.gpg --armor --import publickeyfides.asc
=> import the public key of Fides to encrypt file
gpg --no-default-keyring --keyring .\ZURICHZF.gpg --armor --encrypt --sign -r fides ZURICHZFmessage.txt
=> encrypt end sign file, put the result in the IN folder of decrypt component