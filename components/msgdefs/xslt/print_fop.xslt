<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:template match="root/root">
	<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="sans-serif">
	
		<!-- STD_HEADER -->
	<xsl:variable name="STD_HEADER">
		<fo:block margin-left="150mm" margin-top="0mm">
			<fo:external-graphic scaling="uniform">
				<xsl:attribute name="src">url('<xsl:text disable-output-escaping="yes">/var/isb/images/RPL_RGB_t.png</xsl:text>')</xsl:attribute>
			</fo:external-graphic>
		</fo:block>
	</xsl:variable>
	
		<fo:layout-master-set>
			<fo:simple-page-master margin-right="2.5cm" margin-left="2.5cm" margin-bottom="2cm" margin-top="1cm" page-width="21cm" page-height="29.7cm" master-name="first">
				<fo:region-body margin-bottom="1.5cm" margin-top="1cm"/>
				<fo:region-before region-name="page-header" extent="3cm"/>
				<fo:region-after extent="1.5cm"/>			
			</fo:simple-page-master>
		</fo:layout-master-set>
		<fo:page-sequence master-reference="first">
			<fo:static-content flow-name="page-header">
				<xsl:copy-of select="$STD_HEADER"/>	
			</fo:static-content>>
			<fo:static-content flow-name="xsl-region-after">
				<fo:block text-align="end" font-size="9pt">
					Page <fo:page-number/> of <fo:page-number-citation ref-id="theEnd"/>
				</fo:block>
			</fo:static-content>
			<fo:flow flow-name="xsl-region-body">
				<fo:block text-align="right" font-size="9pt">
					<fo:block padding="0.1in" text-align="start" space-after="1cm" font-family="sans-serif" white-space-collapse="false" wrap-option="wrap">
						<fo:table table-layout="fixed" width="175mm">
							<fo:table-column column-width="20mm"/>
							<fo:table-column column-width="65mm"/>
							<fo:table-column column-width="90mm"/>
							<fo:table-body start-indent="0mm">
								<xsl:for-each select="element">
								  <xsl:if test="not (contains(KEY,'CTRL_'))">
										<xsl:variable name="previousComment"><xsl:value-of select="preceding-sibling::*[1]/COMMENT"/></xsl:variable>
										<xsl:variable name="previousKey"><xsl:value-of select="preceding-sibling::*[1]/KEY"/></xsl:variable>
										<xsl:if test="KEY = 'AckNak'">
											<fo:table-row height="6"><fo:table-cell><fo:block/></fo:table-cell></fo:table-row>
										</xsl:if>
										<xsl:if test="$previousKey = 'Reason'">
											<fo:table-row height="6"><fo:table-cell><fo:block/></fo:table-cell></fo:table-row>
										</xsl:if>
										<fo:table-row>
											<fo:table-cell>
												<fo:block font-weight="bold">
													<xsl:if test="KEY != $previousKey">
														<xsl:value-of select="KEY"/>
													</xsl:if>
												</fo:block> 
											</fo:table-cell>
											<fo:table-cell>
												<fo:block>
													<xsl:if test="COMMENT != $previousComment">
														<xsl:value-of select="COMMENT"/>
													</xsl:if>	
												</fo:block>											
											</fo:table-cell>
											<fo:table-cell>
												<xsl:choose>
													<xsl:when test="'Sender' = $previousKey">
														<fo:block>
															<fo:inline background-color = "yellow">
																<xsl:value-of select="VALUE"/>
															</fo:inline>
														</fo:block>
													</xsl:when>
													<xsl:when test="KEY = 'AckNak'">
														<fo:block>
															<fo:inline background-color = "yellow">
																<xsl:value-of select="VALUE"/>
															</fo:inline>
														</fo:block>
													</xsl:when>
													<xsl:otherwise>
														<fo:block>
															<xsl:value-of select="VALUE"/>
														 </fo:block>
													</xsl:otherwise>
												</xsl:choose> 
											</fo:table-cell>			  
										</fo:table-row> 	
										<xsl:if test="BIC/node()">
											<fo:table-row>
												<fo:table-cell><fo:block/></fo:table-cell>
												<fo:table-cell><fo:block/></fo:table-cell>
												<fo:table-cell>
													<fo:block>
														<xsl:value-of select="BIC"/>
													</fo:block>  
												</fo:table-cell>
											</fo:table-row>			
										</xsl:if>
								  </xsl:if>	
								</xsl:for-each>
							</fo:table-body>
						</fo:table>
					</fo:block>
					<!-- line at the end of the message -->
					<!-- <fo:block border-top="solid black"/> -->
					<!-- end of document marker -->
					<fo:block id="theEnd"/>
				</fo:block>
				<fo:block text-align="start" font-family="sans-serif"  font-size="9pt">
				    ---------------------- end of message -----------------------
				</fo:block>			
			</fo:flow>
		</fo:page-sequence>
	</fo:root>
</xsl:template>
</xsl:stylesheet>