<?xml version="1.0" encoding="UTF-8"?>
<!-- Possible wrapper which binds the existing BAH head.001.001.01 or $ahV10 with the document content inside a valid XML message element -->
<xs:schema xmlns="urn:iso:std:iso:20022:tech:xsd:iso20022wrapperHead.001.001.01" xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="urn:iso:std:iso:20022:tech:xsd:iso20022wrapperHead.001.001.01" elementFormDefault="qualified">
	<xs:element name="message" type="iso20022wrapper"/>
	<xs:complexType name="iso20022wrapper">
		<xs:sequence>
			<xs:element name="Signatures" type="SignatureList" minOccurs="0">
				<xs:annotation>
					<xs:documentation>Optional Signatures block to sign the BAH and Document payloads</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Header" type="ISO20022Header">
				<xs:annotation>
					<xs:documentation>One of the ISO20022 header elements</xs:documentation>
				</xs:annotation>
			</xs:element>
		 <xs:element name="BusinessDocument" type="SwAnyLax" minOccurs="1">
		 	<xs:annotation>
		 		<xs:documentation>Generally contains the IS20022 Document. However this may also contain the Document as binary BLOB to support encrypted data transfer</xs:documentation>
		 	</xs:annotation>
		 </xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SwAnyLax">
		<xs:sequence>
			<xs:any namespace="##any" processContents="lax" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ISO20022Header">
		<xs:choice>
			<xs:element name="AppHdr" type="AppHdr" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The SWIFT application header $ahV10 used in older services such as FUNDs</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="BAH" type="BusinessApplicationHeaderV01" minOccurs="0">
				<xs:annotation>
					<xs:documentation>The new and currently recommended BAH ISO20022 header head.001.001.01 which is using in T2S and other services</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:choice>
	</xs:complexType>
	<xs:simpleType name="AddressType2Code">
		<xs:restriction base="xs:string">
			<xs:enumeration value="ADDR"/>
			<xs:enumeration value="PBOX"/>
			<xs:enumeration value="HOME"/>
			<xs:enumeration value="BIZZ"/>
			<xs:enumeration value="MLTO"/>
			<xs:enumeration value="DLVY"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="AnyBICIdentifier">
		<xs:restriction base="xs:string">
			<xs:pattern value="[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="BICFIIdentifier">
		<xs:restriction base="xs:string">
			<xs:pattern value="[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="BranchAndFinancialInstitutionIdentification5">
		<xs:sequence>
			<xs:element name="FinInstnId" type="FinancialInstitutionIdentification8"/>
			<xs:element name="BrnchId" type="BranchData2" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="BranchData2">
		<xs:sequence>
			<xs:element name="Id" type="Max35Text" minOccurs="0"/>
			<xs:element name="Nm" type="Max140Text" minOccurs="0"/>
			<xs:element name="PstlAdr" type="PostalAddress6" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="BusinessApplicationHeader1">
		<xs:sequence>
			<xs:element name="CharSet" type="UnicodeChartsCode" minOccurs="0"/>
			<xs:element name="Fr" type="Party9Choice"/>
			<xs:element name="To" type="Party9Choice"/>
			<xs:element name="BizMsgIdr" type="Max35Text"/>
			<xs:element name="MsgDefIdr" type="Max35Text"/>
			<xs:element name="BizSvc" type="Max35Text" minOccurs="0"/>
			<xs:element name="CreDt" type="ISONormalisedDateTime"/>
			<xs:element name="CpyDplct" type="CopyDuplicate1Code" minOccurs="0"/>
			<xs:element name="PssblDplct" type="YesNoIndicator" minOccurs="0"/>
			<xs:element name="Prty" type="BusinessMessagePriorityCode" minOccurs="0"/>
			<xs:element name="Sgntr" type="SignatureEnvelope" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SignatureList">
		<xs:sequence>
			<xs:element name="SignaturBAH" type="SignatureEnvelope" minOccurs="0"/>
			<xs:element name="SignaturBusinessDocument" type="SignatureEnvelope" minOccurs="0"/>
			<xs:element name="SignaturBAHAndBusinessDocument" type="SignatureEnvelope" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name = "AppHdr">
		<xs:sequence>
			<xs:element name = "From" type = "EntityIdentification" minOccurs = "0"/>
			<xs:element name = "To" type = "EntityIdentification" minOccurs = "0"/>
			<xs:element name = "SvcName" type = "Max30Text" minOccurs = "0"/>
			<xs:element name = "MsgName" type = "Max30Text" minOccurs = "0"/>
			<xs:element name = "MsgRef" type = "Max30Text"/>
			<xs:element name = "CrDate" type = "ISODateTime"/>
			<xs:element name = "Dup" type = "DuplicateIndication" minOccurs = "0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name = "EntityIdentification">
		<xs:sequence>
			<xs:element name = "Type" type = "Max4Text"/>
			<xs:element name = "Id" type = "Max30Text"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name = "DuplicateIndication">
		<xs:sequence>
			<xs:element name = "Ref" type = "Max30Text"/>
			<xs:element name = "Info" type = "Max140Text"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name = "Max140Text">
		<xs:restriction base = "xs:string">
			<xs:maxLength value = "140"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name = "Max30Text">
		<xs:restriction base = "xs:string">
			<xs:maxLength value = "30"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name = "Max4Text">
		<xs:restriction base = "xs:string">
			<xs:maxLength value = "4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name = "ISODateTime">
		<xs:restriction base = "xs:dateTime"/>
	</xs:simpleType>
	<xs:complexType name="BusinessApplicationHeaderV01">
		<xs:sequence>
			<xs:element name="CharSet" type="UnicodeChartsCode" minOccurs="0"/>
			<xs:element name="Fr" type="Party9Choice" minOccurs="0"/>
			<xs:element name="To" type="Party9Choice" minOccurs="0"/>
			<xs:element name="BizMsgIdr" type="Max35Text"/>
			<xs:element name="MsgDefIdr" type="Max35Text"/>
			<xs:element name="BizSvc" type="Max35Text" minOccurs="0"/>
			<xs:element name="CreDt" type="ISONormalisedDateTime"/>
			<xs:element name="CpyDplct" type="CopyDuplicate1Code" minOccurs="0"/>
			<xs:element name="PssblDplct" type="YesNoIndicator" minOccurs="0"/>
			<xs:element name="Prty" type="BusinessMessagePriorityCode" minOccurs="0"/>
			<xs:element name="Sgntr" type="SignatureEnvelope" minOccurs="0"/>
			<xs:element name="Rltd" type="BusinessApplicationHeader1" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="BusinessMessagePriorityCode">
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
	<xs:complexType name="ClearingSystemIdentification2Choice">
		<xs:sequence>
			<xs:choice>
				<xs:element name="Cd" type="ExternalClearingSystemIdentification1Code"/>
				<xs:element name="Prtry" type="Max35Text"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ClearingSystemMemberIdentification2">
		<xs:sequence>
			<xs:element name="ClrSysId" type="ClearingSystemIdentification2Choice" minOccurs="0"/>
			<xs:element name="MmbId" type="Max35Text"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="ContactDetails2">
		<xs:sequence>
			<xs:element name="NmPrfx" type="NamePrefix1Code" minOccurs="0"/>
			<xs:element name="Nm" type="Max140Text" minOccurs="0"/>
			<xs:element name="PhneNb" type="PhoneNumber" minOccurs="0"/>
			<xs:element name="MobNb" type="PhoneNumber" minOccurs="0"/>
			<xs:element name="FaxNb" type="PhoneNumber" minOccurs="0"/>
			<xs:element name="EmailAdr" type="Max2048Text" minOccurs="0"/>
			<xs:element name="Othr" type="Max35Text" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="CopyDuplicate1Code">
		<xs:restriction base="xs:string">
			<xs:enumeration value="CODU"/>
			<xs:enumeration value="COPY"/>
			<xs:enumeration value="DUPL"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="CountryCode">
		<xs:restriction base="xs:string">
			<xs:pattern value="[A-Z]{2,2}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="DateAndPlaceOfBirth">
		<xs:sequence>
			<xs:element name="BirthDt" type="ISODate"/>
			<xs:element name="PrvcOfBirth" type="Max35Text" minOccurs="0"/>
			<xs:element name="CityOfBirth" type="Max35Text"/>
			<xs:element name="CtryOfBirth" type="CountryCode"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="ExternalClearingSystemIdentification1Code">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="5"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ExternalFinancialInstitutionIdentification1Code">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ExternalOrganisationIdentification1Code">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="ExternalPersonIdentification1Code">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="4"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="FinancialIdentificationSchemeName1Choice">
		<xs:sequence>
			<xs:choice>
				<xs:element name="Cd" type="ExternalFinancialInstitutionIdentification1Code"/>
				<xs:element name="Prtry" type="Max35Text"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="FinancialInstitutionIdentification8">
		<xs:sequence>
			<xs:element name="BICFI" type="BICFIIdentifier" minOccurs="0"/>
			<xs:element name="ClrSysMmbId" type="ClearingSystemMemberIdentification2" minOccurs="0"/>
			<xs:element name="Nm" type="Max140Text" minOccurs="0"/>
			<xs:element name="PstlAdr" type="PostalAddress6" minOccurs="0"/>
			<xs:element name="Othr" type="GenericFinancialIdentification1" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GenericFinancialIdentification1">
		<xs:sequence>
			<xs:element name="Id" type="Max35Text"/>
			<xs:element name="SchmeNm" type="FinancialIdentificationSchemeName1Choice" minOccurs="0"/>
			<xs:element name="Issr" type="Max35Text" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GenericOrganisationIdentification1">
		<xs:sequence>
			<xs:element name="Id" type="Max35Text"/>
			<xs:element name="SchmeNm" type="OrganisationIdentificationSchemeName1Choice" minOccurs="0"/>
			<xs:element name="Issr" type="Max35Text" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="GenericPersonIdentification1">
		<xs:sequence>
			<xs:element name="Id" type="Max35Text"/>
			<xs:element name="SchmeNm" type="PersonIdentificationSchemeName1Choice" minOccurs="0"/>
			<xs:element name="Issr" type="Max35Text" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="ISODate">
		<xs:restriction base="xs:date"/>
	</xs:simpleType>
	<xs:simpleType name="ISONormalisedDateTime">
		<xs:restriction base="xs:dateTime">
			<xs:pattern value=".*Z"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="Max16Text">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="16"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="Max2048Text">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="2048"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="Max35Text">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="35"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="Max70Text">
		<xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="70"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:simpleType name="NamePrefix1Code">
		<xs:restriction base="xs:string">
			<xs:enumeration value="DOCT"/>
			<xs:enumeration value="MIST"/>
			<xs:enumeration value="MISS"/>
			<xs:enumeration value="MADM"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="OrganisationIdentification7">
		<xs:sequence>
			<xs:element name="AnyBIC" type="AnyBICIdentifier" minOccurs="0"/>
			<xs:element name="Othr" type="GenericOrganisationIdentification1" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="OrganisationIdentificationSchemeName1Choice">
		<xs:sequence>
			<xs:choice>
				<xs:element name="Cd" type="ExternalOrganisationIdentification1Code"/>
				<xs:element name="Prtry" type="Max35Text"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Party10Choice">
		<xs:sequence>
			<xs:choice>
				<xs:element name="OrgId" type="OrganisationIdentification7"/>
				<xs:element name="PrvtId" type="PersonIdentification5"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="Party9Choice">
		<xs:sequence>
			<xs:choice>
				<xs:element name="OrgId" type="PartyIdentification42"/>
				<xs:element name="FIId" type="BranchAndFinancialInstitutionIdentification5"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PartyIdentification42">
		<xs:sequence>
			<xs:element name="Nm" type="Max140Text" minOccurs="0"/>
			<xs:element name="PstlAdr" type="PostalAddress6" minOccurs="0"/>
			<xs:element name="Id" type="Party10Choice" minOccurs="0"/>
			<xs:element name="CtryOfRes" type="CountryCode" minOccurs="0"/>
			<xs:element name="CtctDtls" type="ContactDetails2" minOccurs="0"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PersonIdentification5">
		<xs:sequence>
			<xs:element name="DtAndPlcOfBirth" type="DateAndPlaceOfBirth" minOccurs="0"/>
			<xs:element name="Othr" type="GenericPersonIdentification1" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="PersonIdentificationSchemeName1Choice">
		<xs:sequence>
			<xs:choice>
				<xs:element name="Cd" type="ExternalPersonIdentification1Code"/>
				<xs:element name="Prtry" type="Max35Text"/>
			</xs:choice>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="PhoneNumber">
		<xs:restriction base="xs:string">
			<xs:pattern value="\+[0-9]{1,3}-[0-9()+\-]{1,30}"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:complexType name="PostalAddress6">
		<xs:sequence>
			<xs:element name="AdrTp" type="AddressType2Code" minOccurs="0"/>
			<xs:element name="Dept" type="Max70Text" minOccurs="0"/>
			<xs:element name="SubDept" type="Max70Text" minOccurs="0"/>
			<xs:element name="StrtNm" type="Max70Text" minOccurs="0"/>
			<xs:element name="BldgNb" type="Max16Text" minOccurs="0"/>
			<xs:element name="PstCd" type="Max16Text" minOccurs="0"/>
			<xs:element name="TwnNm" type="Max35Text" minOccurs="0"/>
			<xs:element name="CtrySubDvsn" type="Max35Text" minOccurs="0"/>
			<xs:element name="Ctry" type="CountryCode" minOccurs="0"/>
			<xs:element name="AdrLine" type="Max70Text" minOccurs="0" maxOccurs="7"/>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType name="SignatureEnvelope">
		<xs:sequence>
			<xs:any namespace="http://www.w3.org/2000/09/xmldsig#" processContents="lax"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="UnicodeChartsCode">
		<xs:restriction base="xs:string"/>
	</xs:simpleType>
	<xs:simpleType name="YesNoIndicator">
		<xs:restriction base="xs:boolean"/>
	</xs:simpleType>
</xs:schema>
