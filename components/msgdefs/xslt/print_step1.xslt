<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <xsl:text>KEY;VALUE&#10;</xsl:text>
    <xsl:apply-templates />
</xsl:template>
 
<xsl:template match="@*|node()">
  <xsl:for-each select="*">
    <xsl:if test="not(*)">
      <xsl:value-of select="name()"/>;<xsl:call-template name="break"><xsl:with-param name="text" select="."/></xsl:call-template><xsl:text>&#10;</xsl:text>	
    </xsl:if>
  </xsl:for-each>
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>
     
<xsl:template name="break">                         <!-- Converts any CHR(230) characters into <br/> tags for display.  -->
  <xsl:param name="text" select="."/>
  <xsl:choose>
    <xsl:when test="contains($text, '&#230;')">
      <xsl:value-of select="substring-before($text, '&#230;')"/>
      <br/>
      <xsl:call-template name="break">
        <xsl:with-param name="text" select="substring-after($text, '&#230;')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$text"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>