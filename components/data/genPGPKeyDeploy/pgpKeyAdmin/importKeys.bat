REM first clear the keyring to be used
set SCRIPT_HOME=%~dp0
echo %SCRIPT_HOME%
cd %SCRIPT_HOME%


del exportPublicKeys*.*
del *.asc

REM copy all .asc files from the out dir to here
copy ..\out\*.asc .
REM clean out dir
del ..\out\*.asc

REM create the keyring
gpg --no-default-keyring --keyring .\exportPublicKeys.gpg --fingerprint

REM then iterate over all *.asc and import them into the keyring
FOR %%A IN (*.asc) DO (
  gpg --no-default-keyring --keyring .\exportPublicKeys.gpg --armor --import %%A
)

REM Export all public keys into 1 file, ASCII format 
gpg --no-default-keyring --keyring .\exportPublicKeys.gpg --export --armor -o publicKeys.asc

REM -- copy the new publicKeys.asc to the decrypt/encrypt shared keyfolder (windows: not atomic)
copy /Y publicKeys.asc ..\..\..\publickeys\publicKeys.asc 




