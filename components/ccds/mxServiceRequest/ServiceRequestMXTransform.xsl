<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns:n0="urn:iso:std:iso:20022:tech:xsd:admi.002.001.01"
    version="1.0">
    <!-- define global output style -->
    <xsl:output 
        omit-xml-declaration="yes" 
        cdata-section-elements="n0:AddtlData" 
        xml:space="default" 
        indent="no"/>

    <!-- Template to match all nodes, copy them and then apply templates to children. --> 
    <xsl:template match="@*|node()">
        <xsl:copy xml:space="default">
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>