rem -- Script created by Cristian Ionescu (cristian.ionescu@incentage.com)
rem -- It will go through all subdirectories and check if they are empty
rem -- If the directory is empty it will create a .keep file for git
@echo off
setlocal EnableDelayedExpansion
call :treeProcess
goto :eof

set _TMP=false
:treeProcess
for /D %%d in (*) do (
		rem echo Checking in %%d
		set _TMP=false
		for /f "delims=" %%a in ('dir /b "%%d\"') do set _TMP=true
		if {!_TMP!}=={false} (
			echo %%d is empty, creating .keep file
			copy NUL "%%d\.keep"
		)
		
		
		cd %%d 
		rem timeout 2
		call :treeProcess
		cd .. 
	
)
exit /b
